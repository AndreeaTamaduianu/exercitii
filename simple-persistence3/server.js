const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const sequelize = new Sequelize('plane_db', 'app', 'welcome123', {
    dialect : 'mysql'
})

let Plane = sequelize.define('plane', {
    maker : {
        type : Sequelize.ENUM,
        allowNull : false,
        values : ['Boeing', 'Airbus']
    },
    type : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [2-5]
        }
    },
    mass : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            min : 5000
        }
    },
    speed : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            min : 200,
            max : 2500
        }
    }
})

let CrewMember = sequelize.define('crewMember', {
    name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3, 20]
        }
    },
    role : {
        type : Sequelize.ENUM,
        allowNull : false,
        values : ['PILOT', 'COPILOT', 'NAVIGATOR', 'STEWARD']
    }
})

Plane.hasMany(CrewMember)

const app = express()
app.use(bodyParser.json())

app.post('/sync', async (req, res, next) => {
    try {
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

// get /planes?filter=aaa&pageSize=10&page=1
app.get('/planes', async (req, res, next) => {
    try {
        let filter = req.query.filter ? req.query.filter : ''
        let pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10
        let page = req.query.page ? parseInt(req.query.page) : 0
        let planes
        if (page || filter){
            planes = await Plane.findAll({
                where : {
                    maker : {
                        [Op.like] : `%${filter}%`
                    }
                },
                limit : pageSize,
                offset : page * pageSize
            })
        }
        else{
            planes = await Plane.findAll()
        }
        res.status(200).json(planes)
    } catch (err) {
        next(err)
    }    
})

app.post('/planes', async (req, res, next) => {
    try {
        await Plane.create(req.body)
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }    
})

app.get('/planes/:id', async (req, res, next) => {
    try {
        let plane = await Plane.findByPk(req.params.id)
        if (plane){
            res.status(200).json(plane)    
        }
        else{
            res.status(404).json({message : 'not found'})   
        }
    } catch (err) {
        next(err)
    }    
})

app.put('/planes/:id', async (req, res, next) => {
    try {
        let plane = await Plane.findByPk(req.params.id)
        if (plane){
            await plane.update(req.body)
            res.status(202).json({message : 'accepted'})    
        }
        else{
            res.status(404).json({message : 'not found'})   
        }
    } catch (err) {
        next(err)
    }        
})

app.delete('/planes/:id', async (req, res, next) => {
    try {
        let plane = await Plane.findByPk(req.params.id)
        if (plane){
            await plane.destroy()
            res.status(202).json({message : 'accepted'})    
        }
        else{
            res.status(404).json({message : 'not found'})   
        }
    } catch (err) {
        next(err)
    }        
})

app.get('/planes/:pid/crew-members', async (req, res, next) => {
    try {
        let plane = await Plane.findByPk(req.params.pid, {
            include : [CrewMember]
        })
        if (plane){
            res.status(200).json(plane.crewMembers)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.post('/planes/:pid/crew-members', async (req, res, next) => {
    try {
        let plane = await Plane.findByPk(req.params.pid)
        if (plane){
            let crewMember = req.body
            crewMember.planeId = plane.id
            await CrewMember.create(crewMember)
            res.status(201).json({message : 'created'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.get('/planes/:pid/crew-members/:cid', async (req, res, next) => {
    try {
        let plane = await Plane.findByPk(req.params.pid)
        if (plane){
            let crewMembers = await plane.getCrewMembers({
                where : {
                    id : req.params.cid        
                }
            })
            let crewMember = crewMembers.shift()
            if (crewMember){
                res.status(200).json(crewMember)
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.put('/planes/:pid/crew-members/:cid', async (req, res, next) => {
        try {
        let plane = await Plane.findByPk(req.params.pid)
        if (plane){
            let crewMembers = await plane.getCrewMembers({
                where : {
                    id : req.params.cid        
                }
            })
            let crewMember = crewMembers.shift()
            if (crewMember){
                await crewMember.update(req.body, {
                    fields : ['name', 'role']
                })
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.delete('/planes/:pid/crew-members/:cid', async (req, res, next) => {
    try {
        let plane = await Plane.findByPk(req.params.pid)
        if (plane){
            let crewMembers = await plane.getCrewMembers({
                where : {
                    id : req.params.cid        
                }
            })
            let crewMember = crewMembers.shift()
            if (crewMember){
                await crewMember.destroy()
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({message : ':('})
})

app.listen(8080)