import {EventEmitter} from 'fbemitter'
const SERVER = 'http://52.58.55.103:8080'

class BusStore{
    constructor(){
        this.buses = []
        this.emitter = new EventEmitter()
    }
    async getBuses(){
        try{
            let response = await fetch(`${SERVER}/buses`)
            let data = await response.json()
            this.buses = data
            this.emitter.emit('GET_BUSES_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_BUSES_ERROR')
        }
    }
    async addBus(bus){
        try {
            await fetch(`${SERVER}/buses`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(bus)
            })
            this.getBuses()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_BUS_ERROR')            
        }
    }
    async deleteBus(id){
        try {
            await fetch(`${SERVER}/buses/${id}`, {
                method : 'delete'
            })
            this.getBuses()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_BUS_ERROR')            
        }        
    }
    async updateBus(id, bus){
        try {
            await fetch(`${SERVER}/buses/${id}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(bus)
            })
            this.getBuses()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('UPDATE_BUS_ERROR')            
        }        
    }
}

export default BusStore