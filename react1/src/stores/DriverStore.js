import {EventEmitter} from 'fbemitter'
const SERVER = 'http://52.58.55.103:8080'

class DriverStore{
    constructor(){
        this.driver = []
        this.emitter = new EventEmitter()
    }
    async getDrivers(busId){
        try{
            let response = await fetch(`${SERVER}/buses/${busId}/drivers`)
            let data = await response.json()
            this.drivers = data
            this.emitter.emit('GET_DRIVERS_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_DRIVERS_ERROR')
        }
    }
    async addDriver(busId, driver){
        try {
            await fetch(`${SERVER}/buses/${busId}/drivers`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(driver)
            })
            this.getDrivers(busId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_DRIVER_ERROR')            
        }
    }
    async deleteDriver(busId, driverId){
        try {
            await fetch(`${SERVER}/buses/${busId}/drivers/${driverId}`, {
                method : 'delete'
            })
            this.getDrivers(busId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_DRIVER_ERROR')            
        }        
    }
    async updateDriver(busId, driverId, driver){
        try {
            await fetch(`${SERVER}/buses/${busId}/drivers/${driverId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(driver)
            })
            this.getDrivers(busId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('UPDATE_DRIVER_ERROR')            
        }        
    }
}

export default DriverStore