import React,{Component} from 'react'

class DriverForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            shift : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.handleClick = () => {
            this.props.onAdd({
                name : this.state.name,
                shift : this.state.shift
            })
        }
    }

    render(){
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange} />
            <input type="text" placeholder="shift" name="shift" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.handleClick} />
        </div>
    }
}

export default DriverForm