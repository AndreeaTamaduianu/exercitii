import React,{Component} from 'react'

class BusForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            registration : '',
            capacity : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.handleClick = () => {
            this.props.onAdd({
                registration : this.state.registration,
                capacity : this.state.capacity
            })
        }
    }

    render(){
        return <div>
            <input type="text" placeholder="capacity" name="capacity" onChange={this.handleChange} />
            <input type="text" placeholder="registration" name="registration" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.handleClick} />
        </div>
    }
}

export default BusForm