import React, {Component} from 'react'

class Bus extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            capacity : this.props.item.capacity,
            registration : this.props.item.registration
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                registration : this.state.registration,
                capacity : this.state.capacity
            })
            this.setState({
                isEditing : false
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" name="capacity" onChange={this.handleChange} value={this.state.capacity} />
                </h4>
                <h5>
                    <input type="text" name="registration" onChange={this.handleChange} value={this.state.registration} />
                </h5>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.registration}</h4>
                <h5>{item.capacity}</h5>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="select self" onClick={this.select} />
                </div>
            </div>
        }
    }
}

export default Bus