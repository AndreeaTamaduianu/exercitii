import React,{Component} from 'react'
import BusStore from '../stores/BusStore'
import BusForm from './BusForm'
import Bus from './Bus'
import BusDetails from './BusDetails'

class BusList extends Component{
    constructor(){
        super()
        this.state = {
            buses : [],
            selectedBus : null
        }
        this.store = new BusStore()
        this.add = (bus) => {
            this.store.addBus(bus)
        }
        this.delete = (id) => {
            this.store.deleteBus(id)
        }
        this.save = (id, bus) => {
            this.store.updateBus(id, bus)
        }
        this.select = (bus) => {
            this.setState({
                selectedBus : bus
            })
        }
        this.cancel = () => {
            this.setState({
                selectedBus : null
            })
        }
    }
    componentDidMount(){
        this.store.getBuses()
        this.store.emitter.addListener('GET_BUSES_SUCCESS', () => {
            this.setState({
                buses : this.store.buses
            })
        })
    }
    render(){
        if (this.state.selectedBus){
            return <BusDetails item={this.state.selectedBus} onCancel={this.cancel} />   
        }
        else{
            return <div>
                <h3>A list of buses</h3>
                {
                    this.state.buses.map((e, i) => <Bus key={i} item={e} onDelete={this.delete} onSave={this.save} onSelect={this.select} />)
                }
                <BusForm onAdd={this.add} />
            </div>            
        }
    }
}

export default BusList