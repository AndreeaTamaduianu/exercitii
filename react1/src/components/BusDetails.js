import React,{Component} from 'react'
import DriverStore from '../stores/DriverStore'
import DriverForm from './DriverForm'
import Driver from './Driver'

class BusDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
            drivers : []
        }
        this.store = new DriverStore()
        this.add = (driver) => {
            this.store.addDriver(this.props.item.id, driver)
        }
        this.delete = (id) => {
            this.store.deleteDriver(this.props.item.id, id)
        }
        this.save = (id, driver) => {
            this.store.updateDriver(this.props.item.id, id, driver)
        }
        this.cancel = () => {
            this.props.onCancel()
        }
    }
    componentDidMount(){
        this.store.getDrivers(this.props.item.id)
        this.store.emitter.addListener('GET_DRIVERS_SUCCESS', () => {
            this.setState({
                drivers : this.store.drivers
            })
        })
    }
    render(){
        return <div>
            i will be the details for bus {this.props.item.registration}
            <div>
                {
                    this.state.drivers.map((e, i) => <Driver key={i} item={e} onSave={this.save} onDelete={this.delete} />)
                }
            </div>
            <DriverForm onAdd={this.add} />
            <div>
                <input type="button" value="back" onClick={this.cancel} />
            </div>
        </div>
    }
}

export default BusDetails