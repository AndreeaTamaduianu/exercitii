import {EventEmitter} from 'fbemitter'
const SERVER = 'http://3.122.240.243:8080'

class TeamStore{
    constructor(){
        this.teams = []
        this.emitter = new EventEmitter()
    }
    async getTeams(){
        try {
            let response = await fetch(`${SERVER}/teams`)
            let data = await response.json()
            this.teams = data
            this.emitter.emit('GET_TEAMS_SUCCESS')
        } catch (err) {
            console.warn(err)
            this.emitter.emit('GET_TEAMS_ERROR')
        }
    }
    async addTeam(team){
        try {
            await fetch(`${SERVER}/teams`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(team)
            })
            this.getTeams()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_TEAM_ERROR')
        }
    }
    async saveTeam(id, team){
        try {
            await fetch(`${SERVER}/teams/${id}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(team)
            })
            this.getTeams()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('SAVE_TEAM_ERROR')
        }
    }
    async deleteTeam(id){
        try {
            await fetch(`${SERVER}/teams/${id}`, {
                method : 'delete'
            })
            this.getTeams()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_TEAM_ERROR')
        }

    }
}

export default TeamStore