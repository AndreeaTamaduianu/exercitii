import {EventEmitter} from 'fbemitter'
const SERVER = 'http://3.122.240.243:8080'

class PlayerStore{
    constructor(){
        this.players = []
        this.emitter = new EventEmitter()
    }
    async getPlayers(teamId){
        try {
            let response = await fetch(`${SERVER}/teams/${teamId}/players`)
            let data = await response.json()
            this.players = data
            this.emitter.emit('GET_PLAYERS_SUCCESS')
        } catch (err) {
            console.warn(err)
            this.emitter.emit('GET_PLAYERS_ERROR')
        }
    }
    async addPlayer(teamId, player){
        try {
            await fetch(`${SERVER}/teams/${teamId}/players`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(player)
            })
            this.getPlayers(teamId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_PLAYER_ERROR')
        }
    }
}

export default PlayerStore