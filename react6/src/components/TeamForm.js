import React,{Component} from 'react'

class TeamForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            playerCount : ''
        }
        this.add = () => {
            this.props.onAdd({
                name : this.state.name,
                playerCount : this.state.playerCount
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange}/>
            <input type="text" placeholder="playerCount" name="playerCount" onChange={this.handleChange} />
            <input type="button" value="+" onClick={this.add} />
        </div>
    }    
}

export default TeamForm