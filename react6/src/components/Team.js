import React, {Component} from 'react'

class Team extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            name : this.props.item.name,
            playerCount : this.props.item.playerCount
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                name : this.state.name,
                playerCount : this.state.playerCount
            })
            this.setState({
                isEditing : false
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                </h4>
                <div>Player count : 
                    <input type="text" name="playerCount" value={this.state.playerCount} onChange={this.handleChange} />
                </div>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.name}</h4>
                <div>Player count : {item.playerCount}</div>
                <div>
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="select" onClick={this.select} />
                </div>
            </div>
        }
    }
}

export default Team