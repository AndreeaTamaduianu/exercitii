import React, {Component} from 'react'
import PlayerStore from '../stores/PlayerStore'
import PlayerForm from './PlayerForm'

class TeamDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
            players : [],
            selectedTeam : null
        }
        this.store = new PlayerStore()
        this.add = (player) => {
            this.store.addPlayer(this.props.item.id, player)
        }
    }
    componentDidMount(){
        this.store.getPlayers(this.props.item.id)
        this.store.emitter.addListener('GET_PLAYERS_SUCCESS', () => {
            this.setState({
                players : this.store.players
            })
        })
    }
    render(){
        return <div>
            i am the details for {this.props.item.name}
            <div>
                {
                    this.state.players.map((e, i) => <div>
                        {e.name} is a {e.role}
                    </div>)
                }
            </div>
            <PlayerForm onAdd={this.add} />
            <div>
                <input type="button" value="back" onClick={() => this.props.onCancel()} />
            </div>
        </div>
    }
}

export default TeamDetails