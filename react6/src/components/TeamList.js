import React,{Component} from 'react';
import TeamStore from '../stores/TeamStore'
import TeamForm from './TeamForm'
import Team from './Team'
import TeamDetails from './TeamDetails'

class TeamList extends Component{
    constructor(){
        super()
        this.state = {
            teams : [],
            selectedTeam : null
        }
        this.store = new TeamStore()
        this.add = (team) => {
            this.store.addTeam(team)
        }
        this.save = (id, team) => {
            this.store.saveTeam(id, team)
        }
        this.delete = (id) => {
            this.store.deleteTeam(id)
        }
        this.select = (team) => {
            this.setState({
                selectedTeam : team
            })
        }
        this.cancel = () => {
            this.setState({
                selectedTeam : null
            })
        }
    }
    componentDidMount(){
        this.store.getTeams()
        this.store.emitter.addListener('GET_TEAMS_SUCCESS', () => {
            this.setState({
                teams : this.store.teams
            })
        })
    }
    render(){
        if (this.state.selectedTeam){
            return <TeamDetails item={this.state.selectedTeam} onCancel={this.cancel} />
        }
        else{
            return <div>
                {
                    this.state.teams.map((e, i) => <Team key={i} item={e} onDelete={this.delete} onSave={this.save} onSelect={this.select} />)
                }
                <TeamForm onAdd={this.add} />
            </div>            
        }
    }
}

export default TeamList
