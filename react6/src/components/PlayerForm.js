import React,{Component} from 'react'

class PlayerForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            role : ''
        }
        this.add = () => {
            this.props.onAdd({
                name : this.state.name,
                role : this.state.role
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange}/>
            <input type="text" placeholder="role" name="role" onChange={this.handleChange} />
            <input type="button" value="+" onClick={this.add} />
        </div>
    }    
}

export default PlayerForm