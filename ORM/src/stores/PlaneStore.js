import {EventEmitter} from 'fbemitter'

const SERVER = 'http://18.195.230.96:8080'

class PlaneStore{
    constructor(){
        this.planes = []
        this.emitter = new EventEmitter()
    }
    async getPlanes(){
        try{
            let response = await fetch(`${SERVER}/planes`)
            let data = await response.json()
            this.planes = data
            this.emitter.emit('GET_PLANES_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_PLANES_ERROR')
        }
    }
    async addPlane(plane){}
}

export default PlaneStore