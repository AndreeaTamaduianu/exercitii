import React,{Component} from 'react';
import PlaneStore from '../stores/PlaneStore'

class PlaneList extends Component{
  constructor(){
    super()
    this.state = {
      planes : []
    }
    this.planeStore = new PlaneStore()
  }
  componentDidMount(){
    this.planeStore.getPlanes()
    this.planeStore.emitter.addListener('GET_PLANES_SUCCESS',() => {
      this.setState({
        planes : this.planeStore.planes
      })
    })
  }
  render(){
    return <div>
      <h3>A list of planes</h3>
      {
        this.state.planes.map((e, i) => <div key={i}>
          <h4>{e.maker}</h4>
          <h6>with weight {e.mass} and speed {e.speed}</h6>
        </div>)
      }
    </div>
  }
}

export default PlaneList
