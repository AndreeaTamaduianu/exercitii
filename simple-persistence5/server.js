'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const sequelize = new Sequelize('building_db', 'app', 'welcome123', {
    dialect : 'mysql'
})
// https://pastebin.com/TS1bzxJX

const Building = sequelize.define('building', {
    description : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3, 20]
        }
    },
    area : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min : 100
        }
    }
})

const Room = sequelize.define('room', {
    roomKey : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [1, 3],
            is : /[A-Z0-9]+/
        }
    },
    area : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min : 10
        }
    },
    type : {
        type : Sequelize.ENUM,
        allowNull : false,
        values : ['OFFICE', 'MEETING_ROOM']
    }
})

Building.hasMany(Room)

const app = express()
app.use(bodyParser.json())

app.post('/sync', async (req, res, next) => {
    try {
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

// get /buildings?filter=aaa&page=5&pageSize=10
app.get('/buildings', async (req, res, next) => {
    try {
        let filter = req.query.filter ? req.query.filter : ''
        let page = req.query.page ? parseInt(req.query.page) : 0
        let pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10
        let buildings
        if (filter || page){
            buildings = await Building.findAll({
                where : {
                    description : {
                        [Op.like] : `%${filter}%`
                    }
                },
                offset : page * pageSize,
                limit : pageSize
            })
        }
        else{
            buildings = await Building.findAll()
        }
        res.status(200).json(buildings)
    } catch (err) {    
        next(err)
    }    
})

app.post('/buildings', async (req, res, next) => {
    try {
        await Building.create(req.body)
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

app.get('/buildings/:id', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.id)
        if (building){
            res.status(200).json(building)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.put('/buildings/:id', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.id)
        if (building){
            await building.update(req.body)
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.delete('/buildings/:id', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.id)
        if (building){
            await building.destroy()
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
    
})

app.get('/buildings/:bid/rooms', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.bid, {
            include : [Room]
        })
        if (building){
            res.status(200).json(building.rooms)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.post('/buildings/:bid/rooms', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.bid, {
            include : [Room]
        })
        if (building){
            let room = req.body
            room.buildingId = building.id
            await Room.create(room)
            res.status(201).json({message : 'created'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.get('/buildings/:bid/rooms/:rid', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.bid)
        if (building){
            let rooms = await building.getRooms({
                where : {
                    id : req.params.rid
                }
            })
            let room = rooms.shift()
            if (room){
                res.status(200).json(room)
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.put('/buildings/:bid/rooms/:rid', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.bid)
        if (building){
            let rooms = await building.getRooms({
                where : {
                    id : req.params.rid
                }
            })
            let room = rooms.shift()
            if (room){
                await room.update(req.body, {
                    fields : ['type', 'roomKey', 'area']
                })
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.delete('/buildings/:bid/rooms/:rid', async (req, res, next) => {
    try {
        let building = await Building.findByPk(req.params.bid)
        if (building){
            let rooms = await building.getRooms({
                where : {
                    id : req.params.rid
                }
            })
            let room = rooms.shift()
            if (room){
                await room.destroy()
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({message : '>:('})
})

app.listen(8080)