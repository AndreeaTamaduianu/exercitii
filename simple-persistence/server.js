const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const cors = require('cors')

// https://pastebin.com/34WXQxfZ
const app = express()
app.use(cors())
app.use(bodyParser.json())

let sequelize = new Sequelize('maps_db', 'app', 'welcome123', {
    dialect : 'mysql'
})

let Map = sequelize.define('map', {
    description : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [5, 15]
        }
    },
    height : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min : 1
        }
    },
    width : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min : 1
        }
    }
})

let Point = sequelize.define('point', {
    tag : {
        type : Sequelize.STRING,
        allowNull : false
    },
    x : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            min : 0
        }
    },
    y : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            min : 0
        }
    }
})

Map.hasMany(Point)

app.post('/create', async (req, res, next) => {
    try {
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

// get /maps?filter=aaa&page=5&pageSize=10
app.get('/maps', async (req, res, next) => {
    try {
        let condition = {where : {}}
        let filter = req.query.filter ? req.query.filter : ''
        if (filter){
            condition.where.description = {
                [Op.like] : `%${filter}%`
            }
        }
        let pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10
        let page = req.query.page ? parseInt(req.query.page) : 0
        condition.limit = pageSize
        condition.offset = page * pageSize
        console.warn(condition)
        let maps = await Map.findAll(condition)
        res.status(200).json(maps)
    } catch (err) {
        next(err)     
    }    
})

app.post('/maps', async (req, res, next) => {
    try {
        await Map.create(req.body)
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)       
    }
})

app.get('/maps/:id', async (req, res, next) => {
    try {
        let currentMap = await Map.findByPk(req.params.id)
        if (currentMap){
            res.status(200).json(currentMap)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
       next(err)    
    }    
})

app.put('/maps/:id', async (req, res, next) => {
    try {
        let currentMap = await Map.findByPk(req.params.id)
        if (currentMap){
            await currentMap.update(req.body)
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)  
    }    
})

app.delete('/maps/:id', async (req, res, next) => {
    try {
        let currentMap = await Map.findByPk(req.params.id)
        if (currentMap){
            await currentMap.destroy()
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)      
    }    
})

app.get('/maps/:mid/points', async (req, res, next) => {
    try {
        let map = await Map.findByPk(req.params.mid, {
            include : [Point]
        })
        if (map){
            res.status(200).json(map.points)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.post('/maps/:mid/points', async (req, res, next) => {
    try {
        let map = await Map.findByPk(req.params.mid)
        if (map){
            let point = req.body
            point.mapId = map.id
            await Point.create(point)
            res.status(201).json({message : 'created'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.get('/maps/:mid/points/:pid', async (req, res, next) => {
    try {
        let map = await Map.findByPk(req.params.mid)
        if (map){
            let points = await map.getPoints({id : req.params.pid})
            let point = points.shift()
            if (point){
                res.status(200).json(point)
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.put('/maps/:mid/points/:pid', async (req, res, next) => {
    try {
        let map = await Map.findByPk(req.params.mid)
        if (map){
            let points = await map.getPoints({id : req.params.pid})
            let point = points.shift()
            if (point){
                await point.update(req.body, {
                    attributes : ['tag', 'x', 'y']
                })
                res.status(202).json({message: 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.delete('/maps/:mid/points/:pid', async (req, res, next) => {
    try {
        let map = await Map.findByPk(req.params.mid)
        if (map){
            let points = await map.getPoints({id : req.params.pid})
            let point = points.shift()
            if (point){
                await point.destroy()
                res.status(202).json({message: 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }

})

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({message : 'hamster trouble'})
})

app.listen(8080)