import {EventEmitter} from 'fbemitter'
const SERVER = 'http://3.121.237.142:8080'

class RoomStore{
    constructor(){
        this.rooms = []
        this.emitter = new EventEmitter()
    }
    async getRooms(buildingId){
        try{
            let response = await fetch(`${SERVER}/buildings/${buildingId}/rooms`)
            let data = await response.json()
            this.rooms = data
            this.emitter.emit('GET_ROOMS_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_ROOMS_ERROR')
        }
    }
    async addRoom(buildingId, room){
        try{
            await fetch(`${SERVER}/buildings/${buildingId}/rooms`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(room)
            })
            this.getRooms(buildingId)
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('ADD_ROOM_ERROR')
        }
    }
}

export default RoomStore