import {EventEmitter} from 'fbemitter'
const SERVER = 'http://3.121.237.142:8080'

class BuildingStore{
    constructor(){
        this.buildings = []
        this.emitter = new EventEmitter()
    }
    async getBuildings(){
        try{
            let response = await fetch(`${SERVER}/buildings`)
            let data = await response.json()
            this.buildings = data
            this.emitter.emit('GET_BUILDINGS_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_BUILDINGS_ERROR')
        }
    }
    async addBuilding(building){
        try{
            await fetch(`${SERVER}/buildings`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(building)
            })
            this.getBuildings()
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('ADD_BUILDING_ERROR')
        }
    }
    async saveBuilding(id, building){
        try{
            await fetch(`${SERVER}/buildings/${id}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(building)
            })
            this.getBuildings()
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('SAVE_BUILDING_ERROR')
        }
    }
    async deleteBuilding(id){
        try{
            await fetch(`${SERVER}/buildings/${id}`, {
                method : 'delete'
            })
            this.getBuildings()
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('DELETE_BUILDING_ERROR')
        }
    }
}

export default BuildingStore