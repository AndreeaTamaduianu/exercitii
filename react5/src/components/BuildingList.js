import React,{Component} from 'react'
import BuildingStore from '../stores/BuildingStore'
import BuildingForm from './BuildingForm'
import Building from './Building'
import BuildingDetails from './BuildingDetails'

class BuildingList extends Component{
    constructor(){
        super()
        this.state = {
            buildings : [],
            selectedBuilding : null
        }
        this.store = new BuildingStore()
        this.add = (building) => {
            this.store.addBuilding(building)
        }
        this.delete = (id) => {
            this.store.deleteBuilding(id)
        }
        this.save = (id, building) => {
            this.store.saveBuilding(id, building)
        }
        this.select = (building) => {
            this.setState({
                selectedBuilding : building
            })
        }
        this.cancel = () => {
            this.setState({
                selectedBuilding : null
            })
        }
    }
    componentDidMount(){
        this.store.getBuildings()
        this.store.emitter.addListener('GET_BUILDINGS_SUCCESS', () => {
            this.setState({
                buildings : this.store.buildings
            })
        })
    }
    render(){
        if (this.state.selectedBuilding){
            return <BuildingDetails item={this.state.selectedBuilding} onCancel={this.cancel} />   
        }
        else{
            return <div>
                {
                    this.state.buildings.map((e, i) => <Building key={i} item={e} onSave={this.save} onDelete={this.delete} onSelect={this.select} />)
                } 
                <BuildingForm onAdd={this.add} />
            </div>            
        }
    }
}

export default BuildingList
