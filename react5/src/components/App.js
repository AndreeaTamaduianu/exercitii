import React,{Component} from 'react'
import BuildingList from './BuildingList'

class App extends Component{
  render(){
    return <div>
      <BuildingList />
    </div>
  }
}

export default App
