import React,{Component} from 'react'

class BuildingForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            description : '',
            area : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.handleClick = () => {
            this.props.onAdd({
                description : this.state.description,
                area : this.state.area
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="description" name="description" onChange={this.handleChange} />
            <input type="text" placeholder="area" name="area" onChange={this.handleChange} />
            <input type="button" value="+" onClick={this.handleClick} />
        </div>
    }
}

export default BuildingForm