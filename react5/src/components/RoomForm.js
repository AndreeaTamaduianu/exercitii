import React,{Component} from 'react'

class RoomForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            roomKey : '',
            area : '',
            type : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.handleClick = () => {
            this.props.onAdd({
                roomKey : this.state.roomKey,
                area : this.state.area,
                type : this.state.type
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="room key" name="roomKey" onChange={this.handleChange} />
            <input type="text" placeholder="area" name="area" onChange={this.handleChange} />
            <input type="text" placeholder="type" name="type" onChange={this.handleChange} />
            <input type="button" value="+" onClick={this.handleClick} />
        </div>
    }
}

export default RoomForm