import React, {Component} from 'react'

class Building extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            description : this.props.item.description,
            area : this.props.item.area
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                description : this.state.description,
                area : this.state.area
            })
            this.setState({
                isEditing : false
            })
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" name="description" value={this.state.description} onChange={this.handleChange} />
                </h4>
                <h5>Area : 
                    <input type="text" name="area" value={this.state.area} onChange={this.handleChange} />
                </h5>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />                
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.description}</h4>
                <h5>Area : {item.area}</h5>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="select" onClick={this.select} />
                </div>
            </div>            
        }
    }
}

export default Building