import React, {Component} from 'react'
import RoomStore from '../stores/RoomStore'
import RoomForm from './RoomForm'

class BuildingDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
            rooms : [],
        }
        this.store = new RoomStore()
        this.add = (room) => {
            this.store.addRoom(this.props.item.id, room)
        }
    }
    componentDidMount(){
        this.store.getRooms(this.props.item.id)
        this.store.emitter.addListener('GET_ROOMS_SUCCESS', () => {
            this.setState({
                rooms : this.store.rooms
            })
        })
    }

    render(){
        return <div>
            i am the details page for {this.props.item.description}
            <div>
                {
                    this.state.rooms.map((e, i) => <div key={i}>{e.roomKey}</div>)
                }
            </div>
            <RoomForm onAdd={this.add} />
            <div>
                <input type="button" value="back" onClick={() => this.props.onCancel()} />
            </div>
        </div>
    }
}

export default BuildingDetails