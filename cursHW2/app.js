function addTokens(input, tokens){
    if(typeof input ==='number')
    {
        throw new Error("Invalid input");    
    }
    else{
        if(input.length <7 )
        throw new Error("Input should have at least 6 characters")
        
    }
        var object = tokens[0].tokenName;
        if(typeof object !=='string')
        throw new Error("Invalid array format");
        
        if(input.includes("...")){
            input=input.replace('...',"${"+object.toString()+"}");
        return input;}
            else return input;
        
    
}

const app = {
    addTokens: addTokens
}

module.exports = app;