import React,{Component} from 'react'

class RiderForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            accesibleOn : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.handleClick = () => {
            this.props.onAdd({
                name : this.state.name,
                accesibleOn : this.state.accesibleOn
            })
        }
    }

    render(){
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange} />
            <input type="text" placeholder="accesibleOn" name="accesibleOn" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.handleClick} />
        </div>
    }
}

export default RiderForm