import React,{Component} from 'react'
import BikeStore from '../stores/BikeStore'
import BikeForm from './BikeForm'
import Bike from './Bike'
import BikeDetails from './BikeDetails'

class BikeList extends Component{
  constructor(){
    super()
    this.state = {
      bikes : [],
      selectedBike : null
    }
    this.bikeStore = new BikeStore()
    this.add = (bike) => {
      this.bikeStore.addBike(bike)
    }
    this.delete = (id) => {
      this.bikeStore.deleteBike(id)
    }
    this.save = (id, bike) => {
      this.bikeStore.saveBike(id, bike)
    }
    this.select = (bike) => {
      this.setState({
        selectedBike : bike
      })
    }
    this.cancel = () => {
      this.setState({
        selectedBike : null
      })
    }
  }
  componentDidMount(){
    this.bikeStore.getBikes()
    this.bikeStore.emitter.addListener('GET_BIKES_SUCCESS',() => {
      this.setState({
        bikes : this.bikeStore.bikes
      })
    })
  }
  render(){
    if (this.state.selectedBike){
      return <BikeDetails item={this.state.selectedBike} onCancel={this.cancel} />
    }
    else{
      return <div>
        <h3>A list of bikes</h3>
        {
          this.state.bikes.map((e, i) => <Bike key={i} item={e} onDelete={this.delete} onSave={this.save} onSelect={this.select} />)
        }
        <BikeForm onAdd={this.add} />
      </div>      
    }
  }
}

export default BikeList
