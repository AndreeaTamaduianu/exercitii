import React,{Component} from 'react'

class BikeForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            gears : '',
            weight : '',
            brand : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.handleClick = () => {
            this.props.onAdd({
                gears : this.state.gears,
                weight : this.state.weight,
                brand : this.state.brand
            })
        }
    }

    render(){
        return <div>
            <input type="text" placeholder="brand" name="brand" onChange={this.handleChange} />
            <input type="text" placeholder="gears" name="gears" onChange={this.handleChange} />
            <input type="text" placeholder="weight" name="weight" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.handleClick} />
        </div>
    }
}

export default BikeForm