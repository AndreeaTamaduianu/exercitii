import React, {Component} from 'react'
import './Rider.css'

class Rider extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            name : this.props.item.name,
            accesibleOn : this.props.item.accesibleOn
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                name : this.state.name,
                accesibleOn : this.state.accesibleOn
            })
            this.setState({
                isEditing : false
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>I am 
                    <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                </h4>
                <h6>and i have access t be bike on 
                    <input type="text" name="accesibleOn" value={this.state.accesibleOn} onChange={this.handleChange} /> 
                    days
                </h6>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
              <h4  className="i-am-orange">I am {item.name}</h4>
              <h6>and i have access to the bike on {item.accesibleOn} days</h6>
              <div>
                <input type="button" value="delete" onClick={this.delete} />
                <input type="button" value="edit" onClick={this.edit} />
              </div>
            </div>
        }
    }
}

export default Rider