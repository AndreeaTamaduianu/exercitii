import React,{Component} from 'react'
import RiderStore from '../stores/RiderStore'
import RiderForm from './RiderForm'
import Rider from './Rider'

class BikeDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
            riders : []
        }
        this.riderStore = new RiderStore()
        this.add = (rider) => {
          this.riderStore.addRider(this.props.item.id, rider)
        }
        this.delete = (id) => {
          this.riderStore.deleteRider(this.props.item.id, id)
        }
        this.save = (id, rider) => {
          this.riderStore.saveRider(this.props.item.id, id, rider)
        }
        this.cancel = () => {
            this.props.onCancel()
        }
    }
    componentDidMount(){
        this.riderStore.getRiders(this.props.item.id)
        this.riderStore.emitter.addListener('GET_RIDERS_SUCCESS',() => {
            this.setState({
                riders : this.riderStore.riders
            })
        })
    }
    render(){
        return <div>
            i will be the details for a {this.props.item.brand} bike
            <div>
                {this.state.riders.map((e, i) => <Rider key={e.id} item={e} onSave={this.save} onDelete={this.delete} />)}
            </div>
            <RiderForm onAdd={this.add} />
            <div>
                <input type="button" value="back" onClick={this.cancel} />
            </div>
        </div>
    }
}

export default BikeDetails