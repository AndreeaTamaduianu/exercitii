import React, {Component} from 'react'

class Bike extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            brand : this.props.item.brand,
            weight : this.props.item.weight,
            gears : this.props.item.gears
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                brand : this.state.brand,
                weight : this.state.weight,
                gears : this.state.gears
            })
            this.setState({
                isEditing : false
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" name="brand" value={this.state.brand} onChange={this.handleChange} />
                </h4>
                <h6>with weight  
                    <input type="text" name="weight" value={this.state.weight} onChange={this.handleChange} /> 
                    and gears 
                    <input type="text" name="gears" value={this.state.gears} onChange={this.handleChange} />
                    </h6>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
              <h4>{item.brand}</h4>
              <h6>with weight {item.weight} and gears {item.gears}</h6>
              <div>
                <input type="button" value="delete" onClick={this.delete} />
                <input type="button" value="edit" onClick={this.edit} />
                <input type="button" value="select" onClick={this.select} />
              </div>
            </div>
        }
    }
}

export default Bike