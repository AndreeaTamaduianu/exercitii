import {EventEmitter} from 'fbemitter'

const SERVER = 'http://52.58.55.103:8080'

class BikeStore{
    constructor(){
        this.bikes = []
        this.emitter = new EventEmitter()
    }
    async getBikes(){
        try{
            let response = await fetch(`${SERVER}/bikes`)
            let data = await response.json()
            this.bikes = data
            this.emitter.emit('GET_BIKES_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_BIKES_ERROR')
        }
    }
    async addBike(bike){
        try {
            await fetch(`${SERVER}/bikes`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(bike)
            })
            this.getBikes()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_BIKE_ERROR')            
        }
    }
    async deleteBike(id){
        try {
            await fetch(`${SERVER}/bikes/${id}`, {
                method : 'delete'
            })
            this.getBikes()
        }
        catch (err){
            console.warn(err)
            this.emitter.emit('DELETE_BIKE_ERROR')
        }
    }
    async saveBike(id, bike){
        try {
            await fetch(`${SERVER}/bikes/${id}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(bike)
            })
            this.getBikes()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('SAVE_BIKE_ERROR')            
        }        
    }
}

export default BikeStore