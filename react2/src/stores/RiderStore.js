import {EventEmitter} from 'fbemitter'

const SERVER = 'http://52.58.55.103:8080'

class RiderStore{
    constructor(){
        this.riders = []
        this.emitter = new EventEmitter()
    }
    async getRiders(bikeId){
        try{
            let response = await fetch(`${SERVER}/bikes/${bikeId}/riders`)
            let data = await response.json()
            this.riders = data
            this.emitter.emit('GET_RIDERS_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_RIDERS_ERROR')
        }
    }
    async addRider(bikeId, rider){
        try {
            await fetch(`${SERVER}/bikes/${bikeId}/riders`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(rider)
            })
            this.getRiders(bikeId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_RIDER_ERROR')            
        }
    }
    async deleteRider(bikeId, riderId){
        try {
            await fetch(`${SERVER}/bikes/${bikeId}/riders/${riderId}`, {
                method : 'delete'
            })
            this.getRiders(bikeId)
        }
        catch (err){
            console.warn(err)
            this.emitter.emit('DELETE_RIDER_ERROR')
        }
    }
    async saveRider(bikeId, riderId, rider){
        try {
            await fetch(`${SERVER}/bikes/${bikeId}/riders/${riderId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(rider)
            })
            this.getRiders(bikeId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('SAVE_RIDER_ERROR')            
        }        
    }
}

export default RiderStore