const FIRST_NAME = "Dana Cristina";
const LAST_NAME = "Comișelu";
const GRUPA = "1090";

function numberParser(value) {
     if(value===NaN)
     return NaN;

     if(value===Infinity||value===-Infinity)
     return NaN;

     if(value<Number.MIN_SAFE_INTEGER)
        return Number.MIN_VALUE;
     else if(value>Number.MAX_SAFE_INTEGER)
        return NaN;
     if(typeof(value)=='string')
     return parseInt(value);
     if(typeof(value)=='number')
     return Number.parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}