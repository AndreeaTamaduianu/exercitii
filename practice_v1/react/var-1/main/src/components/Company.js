import React, { Component } from 'react'

class Company extends Component {
	constructor(props){
		super(props)
		let {item} = this.props
		this.state = {
			name : item.name,
			employees : item.employees,
			revenue : item.revenue,
			isEditing:false
		}
		this.handleChange = (evt) => {
			this.setState({
				[evt.target.name] : evt.target.value
			})
		}
		this.save = () => {
     this.props.onSave(this.props.item.id, {
         name : this.state.name,
         employees : this.state.employees,
         revenue:this.state.revenue})
      this.setState({
          isEditing : false})
    }
    this.edit = () => {
      this.setState({
        isEditing : true})
    }
    this.cancel = () => {
      this.setState({
        isEditing : false})
    }
	}
  render() {
    let {item} = this.props
    if (this.state.isEditing){
      return (
        <div>
            <h4>Name :
            <input type="text" value={this.state.name} name="name" id="name" onChange={this.handleChange} />
            </h4>
            <h5>Employees : 
            <input type="text" value={this.state.employees} name="employees" id="employees" onChange={this.handleChange} />
            </h5>
            <h5>Revenue : 
            <input type="text" value={this.state.revenue} name="revenue" id="revenue" onChange={this.handleChange} />
            </h5>
          <input type="button" value="save" onClick={this.save} />
          <input type="button" value="cancel" onClick={this.cancel}/>						
        </div>
      )
    }
    else{
      return (
        <div>
          Name {item.name} with {item.employees} employees {item.revenue} revenue
          <input type="button" value="edit" onClick={this.edit} />        
        </div>
      )
    }
  }
}

//<input type="button" value="edit" onClick={()=>this.setState({isEditing : true})} />
export default Company
