function calculateFrequencies(input, dictionary){
    if (typeof input != "string") {
    throw new Error("Input should be a string");
  }
  dictionary.forEach(word => {
    if (typeof word != "string") {
      throw new Error("Invalid dictionary format");
    }
  });
    var dict = new Object();
    let sentence1=input.split(' ');
    let uniqueSet=[...new Set(sentence1)];
    
    let myArr = Array.from(uniqueSet);
    
    let sentence=sentence1.filter(function(value){
        if(dictionary.indexOf(value.toLowerCase())>=0 || dictionary.indexOf(value)>=0){
            return false;}
        else return true;
    })
    console.log(sentence);
    
    for (let index = 0; index < sentence.length; index++) { 
        //console.log(myArr[index]); 
        dict[sentence[index].toLowerCase()]=0;
    } 
    
    let percentage=sentence.length;
    
    for(let index=0;index<sentence.length;index++){
        let word=sentence[index].toLowerCase();
        let p=0;
        for(let indexj=0;indexj<myArr.length;indexj++){
            if(myArr[indexj].toLowerCase()===word)
            p++;
        }
        //console.log(word);
        dict[word]=p/percentage;
    }
    //console.log(dict);
    return dict;
}

const app = {
    calculateFrequencies
};

// if(dict.hasOwnProperty(wordInput))
//    { 
//         dict[wordInput] ++;
//         nr++;
//    }

module.exports = app;