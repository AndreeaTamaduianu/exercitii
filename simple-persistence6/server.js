'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('team_db', 'app', 'welcome123', {
    dialect : 'mysql'
})

const Team = sequelize.define('team', {
    name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3-20],
            is : /^[0-9a-zA-Z]+$/
        }
    },
    playerCount : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min : 2
        }
    }
})

const Player = sequelize.define('player', {
    name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3, 40],
            is : /^[a-zA-Z]+$/
        }
    },
    role : {
        type : Sequelize.ENUM,
        allowNull : false,
        values : ['CAPTAIN', 'PLAYER']
    }
})

Team.hasMany(Player)

const app = express()
app.use(bodyParser.json())

app.post('/sync', async(req, res, next) => {
    try {
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

// get /teams?filter=aa&page=1&pageSize=5
app.get('/teams', async (req, res, next) => {
    try {
        let params = {}
        if (req.query.filter){
            params.where = {
                name : {
                    [Sequelize.Op.like] : `%${req.query.filter}%`
                }
            }
        }
        if (req.query.page){
            let page = parseInt(req.query.page)
            let pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10
            params.limit = pageSize
            params.offset  = pageSize * page
        }
        let teams = await Team.findAll(params)
        res.status(200).json(teams)
    } catch (err) {
        next(err)
    }    
})

app.post('/teams', async (req, res, next) => {
    try {
        await Team.create(req.body)
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

app.get('/teams/:id', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.id)
        if (team){
            res.status(200).json(team)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.put('/teams/:id', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.id)
        if (team){
            await team.update(req.body)
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.delete('/teams/:id', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.id)
        if (team){
            await team.destroy()
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.get('/teams/:tid/players', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.tid, {
            include : [Player]
        })
        if (team){
            res.status(200).json(team.players)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.post('/teams/:tid/players', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.tid)
        if (team){
            let player = req.body
            player.teamId = team.id
            await Player.create(player)
            res.status(201).json({message : 'created'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.get('/teams/:tid/players/:pid', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.tid)
        if (team){
            let players = await team.getPlayers({
                where : {
                    id : req.params.pid
                }
            })
            let player = players.shift()
            if (player){
                res.status(200).json(player)
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }    
})


app.put('/teams/:tid/players/:pid', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.tid)
        if (team){
            let players = await team.getPlayers({
                where : {
                    id : req.params.pid
                }
            })
            let player = players.shift()
            if (player){
                await player.update(req.body, {
                    fields : ['name', 'role']
                })
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }    
})

app.delete('/teams/:tid/players/:pid', async (req, res, next) => {
    try {
        let team = await Team.findByPk(req.params.tid)
        if (team){
            let players = await team.getPlayers({
                where : {
                    id : req.params.pid
                }
            })
            let player = players.shift()
            if (player){
                await player.destroy()
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }   
})

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({message : ':('})
})

app.listen(8080)
