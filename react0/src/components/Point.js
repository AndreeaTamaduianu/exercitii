import React, {Component} from 'react'

class Point extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            tag : this.props.item.tag,
            x : this.props.item.x,
            y : this.props.item.y
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                tag : this.state.tag,
                x : this.state.x,
                y : this.state.y
            })
            this.setState({
                isEditing : false
            })
        }

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <div>
                    <input type="text" name="tag" onChange={this.handleChange} value={this.state.tag} />
                    <input type="text" name="x" onChange={this.handleChange} value={this.state.x} />
                    <input type="text" name="y" onChange={this.handleChange} value={this.state.y} />
                </div>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                Point {item.tag} at {item.x} and {item.y}
                <div>
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="delete" onClick={this.delete} />
                </div>
            </div>
        }
    }
}

export default Point