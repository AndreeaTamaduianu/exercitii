import React, {Component} from 'react'

class PointForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            x : '',
            y : '',
            tag : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.add = () => {
            this.props.onAdd({
                x : this.state.x,
                y : this.state.y,
                tag : this.state.tag
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="x" name="x" onChange={this.handleChange} />
            <input type="text" placeholder="y" name="y" onChange={this.handleChange} />
            <input type="text" placeholder="tag" name="tag" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.add} />  
        </div>
    }
}

export default PointForm