import React, {Component} from 'react'
import MapList from './MapList'

class App extends Component {
  render(){
    return (
      <div>
        <MapList />
      </div>
    )
  }
}

export default App
