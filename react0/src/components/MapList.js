import React, {Component} from 'react'
import MapStore from '../stores/MapStore'
import MapForm from './MapForm'
import Map from './Map'
import MapDetails from './MapDetails'

class MapList extends Component{
    constructor(){
        super()
        
        this.state = {
            maps : [],
            selectedId : -1,
            selectedMap : null
        }
        
        this.mapStore = new MapStore()
        
        this.add = (map) => {
            this.mapStore.addMap(map)
        }
        this.delete = (id) => {
            this.mapStore.deleteMap(id)
        }
        this.save = (id, map) => {
            this.mapStore.saveMap(id, map)
        }
        this.select = (id) => {
            let selectedMap = this.state.maps.find((e) => e.id === id)
            this.setState({
                selectedId : id,
                selectedMap : selectedMap
            })
        }
    }
    componentDidMount(){
        this.mapStore.getMaps()
        this.mapStore.emitter.addListener('GET_MAPS_SUCCESS', () => {
            this.setState({
                maps : this.mapStore.maps
            })
        })
    }
    // https://eu-central-1.console.aws.amazon.com/cloud9/ide/6043a071db5c4987a8c7b056bd03b34e
    render(){
        if (this.state.selectedId === -1){
            return <div>
                <div>
                    A list of maps
                </div>
                {
                    this.state.maps.map((e, i) => <Map key={i} item={e} onDelete={this.delete} onSave={this.save} onSelect={this.select} />)
                }
                <div>
                    <MapForm onAdd={this.add} />
                </div>
            </div>            
        }
        else{
            return <MapDetails item={this.state.selectedMap} />
        }
    }
}

export default MapList