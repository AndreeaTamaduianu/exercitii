import React,{Component} from 'react'
import PointStore from '../stores/PointStore'
import PointForm from './PointForm'
import Point from './Point'

class MapDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
            points : []
        }
        
        this.pointStore = new PointStore()
        
        this.add = (point) => {
            this.pointStore.addPoint(this.props.item.id, point)
        }
        this.delete = (id) => {
            this.pointStore.deletePoint(this.props.item.id, id)
        }
        this.save = (id, point) => {
            this.pointStore.savePoint(this.props.item.id, id, point)
        }
    }
    componentDidMount(){
        this.pointStore.getPoints(this.props.item.id)
        this.pointStore.emitter.addListener('GET_POINTS_SUCCESS', () => {
            this.setState({
                points : this.pointStore.points
            })
        })
    }
    render(){
        return <div>
            i will be the map details for {this.props.item.description}
            {
                this.state.points.map((e, i) => <Point key={i} item={e} onSave={this.save} onDelete={this.delete}  />)
            }
            <PointForm onAdd={this.add} />
        </div>
    }
}

export default MapDetails