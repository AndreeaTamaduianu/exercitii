import React, {Component} from 'react'

class MapForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            description : '',
            height : '',
            width : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.add = () => {
            this.props.onAdd({
                description : this.state.description,
                height : this.state.height,
                width : this.state.width
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="description" name="description" onChange={this.handleChange} />
            <input type="text" placeholder="width" name="width" onChange={this.handleChange} />
            <input type="text" placeholder="height" name="height" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.add} />  
        </div>
    }
}

export default MapForm