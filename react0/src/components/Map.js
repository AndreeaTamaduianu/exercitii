import React, {Component} from 'react'

class Map extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            description : this.props.item.description,
            width : this.props.item.width,
            height : this.props.item.height
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                description : this.state.description,
                height : this.state.height,
                width : this.state.width
            })
            this.setState({
                isEditing : false
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item.id)
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <div>
                    <input type="text" name="description" onChange={this.handleChange} value={this.state.description} />
                    <input type="text" name="width" onChange={this.handleChange} value={this.state.width} />
                    <input type="text" name="height" onChange={this.handleChange} value={this.state.height} />
                </div>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                A map of {item.description} of size {item.width}X{item.height}
                <div>
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="select" onClick={this.select} />
                </div>
            </div>
        }
    }
}

export default Map