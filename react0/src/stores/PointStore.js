import {EventEmitter} from 'fbemitter'

const SERVER = 'http://35.158.124.29:8080'

class PointStore{
    constructor(){
        this.points = []
        this.emitter = new EventEmitter()
    }
    async getPoints(mapId){
        try{
            let response = await fetch(`${SERVER}/maps/${mapId}/points`)
            let data = await response.json()
            this.points = data
            this.emitter.emit('GET_POINTS_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_POINTS_ERROR')
        }
    }
    async addPoint(mapId, point){
        try{
            await fetch(`${SERVER}/maps/${mapId}/points`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(point)
            })
            this.getPoints(mapId)
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('ADD_POINTS_ERROR')
        }
    }
    async deletePoint(mapId, pointId){
        try{
            await fetch(`${SERVER}/maps/${mapId}/points/${pointId}`, {
                method : 'delete'
            })
            this.getPoints(mapId)
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('ADD_POINTS_ERROR')
        }
    }
    async savePoint(mapId, pointId, point){
        try{
            await fetch(`${SERVER}/maps/${mapId}/points/${pointId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(point)
            })
            this.getPoints(mapId)
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('SAVE_POINT_ERROR')
        }
    }
}

export default PointStore