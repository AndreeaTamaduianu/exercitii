import {EventEmitter} from 'fbemitter'

const SERVER = 'http://35.158.124.29:8080'

class MapStore{
    constructor(){
        this.maps = []
        this.emitter = new EventEmitter()
    }
    async getMaps(){
        try{
            let response = await fetch(`${SERVER}/maps`)
            let data = await response.json()
            this.maps = data
            this.emitter.emit('GET_MAPS_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_MAPS_ERROR')
        }
    }
    async addMap(map){
        try{
            await fetch(`${SERVER}/maps`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(map)
            })
            this.getMaps()
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('ADD_MAPS_ERROR')
        }
    }
    async deleteMap(id){
        try{
            await fetch(`${SERVER}/maps/${id}`, {
                method : 'delete'
            })
            this.getMaps()
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('ADD_MAPS_ERROR')
        }
    }
    async saveMap(id, map){
        try{
            await fetch(`${SERVER}/maps/${id}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(map)
            })
            this.getMaps()
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('ADD_MAPS_ERROR')
        }
    }
}

export default MapStore