import {EventEmitter} from 'fbemitter'

const SERVER = 'http://18.195.215.120:8080'

class CrewStore{
    constructor(){
        this.crewMembers = []
        this.emitter = new EventEmitter()
    }
    async getCrewMembers(planeId){
        try{
            let response = await fetch(`${SERVER}/planes/${planeId}/crew-members`)
            let data = await response.json()
            this.crewMembers = data
            this.emitter.emit('GET_CREW_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_CREW_ERROR')
        }
    }
    async addCrewMember(planeId, crewMember){
        try {
            await fetch(`${SERVER}/planes/${planeId}/crew-members`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(crewMember)
            })
            this.getCrewMembers(planeId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_CREW_ERROR')
        }
    }
    async deleteCrewMember(planeId, crewMemberId){
        try {
            await fetch(`${SERVER}/planes/${planeId}/crew-members/${crewMemberId}`, {
                method : 'delete'
            })
            this.getCrewMembers(planeId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_CREW_ERROR')
        }
    }
    async saveCrewMember(planeId, crewMemberId, crewMember){
        try {
            await fetch(`${SERVER}/planes/${planeId}/crew-members/${crewMemberId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(crewMember)
            })
            this.getCrewMembers(planeId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('UPDATE_CREW_ERROR')
        }
    }
}

export default CrewStore