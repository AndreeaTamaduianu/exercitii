import {EventEmitter} from 'fbemitter'

const SERVER = 'http://18.195.215.120:8080'

class PlaneStore{
    constructor(){
        this.planes = []
        this.emitter = new EventEmitter()
    }
    async getPlanes(){
        try{
            let response = await fetch(`${SERVER}/planes`)
            let data = await response.json()
            this.planes = data
            this.emitter.emit('GET_PLANES_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_PLANES_ERROR')
        }
    }
    async addPlane(plane){
        try {
            await fetch(`${SERVER}/planes`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(plane)
            })
            this.getPlanes()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_PLANE_ERROR')
        }
    }
    async deletePlane(id){
        try {
            await fetch(`${SERVER}/planes/${id}`, {
                method : 'delete'
            })
            this.getPlanes()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_PLANE_ERROR')
        }
    }
    async savePlane(id, plane){
        try {
            await fetch(`${SERVER}/planes/${id}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(plane)
            })
            this.getPlanes()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('UPDATE_PLANE_ERROR')
        }
    }
}

export default PlaneStore