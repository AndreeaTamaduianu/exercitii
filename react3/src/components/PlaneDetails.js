import React, {Component} from 'react'
import CrewStore from '../stores/CrewStore'
import CrewMember from './CrewMember'
import CrewForm from './CrewForm'

class PlaneDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
          crewMembers : []
        }
        this.crewStore = new CrewStore()
        this.add = (crewMember) => {
          this.crewStore.addCrewMember(this.props.item.id, crewMember)
        }
        this.delete = (crewMemberId) => {
          this.crewStore.deleteCrewMember(this.props.item.id, crewMemberId)
        }
        this.save = (crewMemberId, crewMember) => {
          this.crewStore.saveCrewMember(this.props.item.id, crewMemberId, crewMember)
        }
    }
    componentDidMount(){
        this.crewStore.getCrewMembers(this.props.item.id)
        this.crewStore.emitter.addListener('GET_CREW_SUCCESS', () => {
          this.setState({
            crewMembers : this.crewStore.crewMembers
          })
        })
    }
    render(){
        let {item} = this.props
        return <div>
            i show the details for {item.maker} - {item.type}
            <div>
                {
                  this.state.crewMembers.map((e, i) => <CrewMember key={i} item={e} onDelete={this.delete} onSave={this.save} />)
                }
                <CrewForm onAdd={this.add} />
            </div>
            <div>
                <input type="button" value="back" onClick={() => this.props.onCancel()} />
            </div>
        </div>
    }
}

export default PlaneDetails