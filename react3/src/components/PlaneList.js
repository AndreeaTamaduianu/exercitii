import React,{Component} from 'react';
import PlaneStore from '../stores/PlaneStore'
import PlaneForm from './PlaneForm'
import Plane from './Plane'
import PlaneDetails from './PlaneDetails'

class PlaneList extends Component{
  constructor(){
    super()
    this.state = {
      planes : [],
      selectedPlane : null
    }
    this.planeStore = new PlaneStore()
    this.add = (plane) => {
      this.planeStore.addPlane(plane)
    }
    this.delete = (id) => {
      this.planeStore.deletePlane(id)
    }
    this.save = (id, plane) => {
      this.planeStore.savePlane(id, plane)
    }
    this.select = (plane) => {
      this.setState({
        selectedPlane : plane
      })
    }
    this.cancel = () => {
      this.setState({
        selectedPlane : null
      })
    }
  }
  componentDidMount(){
    this.planeStore.getPlanes()
    this.planeStore.emitter.addListener('GET_PLANES_SUCCESS',() => {
      this.setState({
        planes : this.planeStore.planes
      })
    })
  }
  render(){
    if (this.state.selectedPlane){
      return <PlaneDetails item={this.state.selectedPlane} onCancel={this.cancel} />
    }
    else{
      return <div>
        <h3>A list of planes</h3>
        {
          this.state.planes.map((e, i) => <Plane key={i} item={e} onDelete={this.delete} onSave={this.save} onSelect={this.select} />)
        }
        <PlaneForm onAdd={this.add} />
      </div>      
    }
  }
}

export default PlaneList
