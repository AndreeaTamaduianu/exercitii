import React, {Component} from 'react'

class PlaneForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            maker : '',
            type : '',
            mass : '',
            speed : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.add = () => {
            this.props.onAdd({
                maker : this.state.maker,
                type : this.state.type,
                mass : this.state.mass,
                speed : this.state.speed
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="maker" name="maker" onChange={this.handleChange} />
            <input type="text" placeholder="type" name="type" onChange={this.handleChange} /> 
            <input type="text" placeholder="mass" name="mass" onChange={this.handleChange} />
            <input type="text" placeholder="speed" name="speed" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.add} />
        </div>
    }
}

export default PlaneForm