import React, {Component} from 'react'

class CrewForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            role : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.add = () => {
            this.props.onAdd({
                name : this.state.name,
                role : this.state.role
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange} />
            <input type="text" placeholder="role" name="role" onChange={this.handleChange} /> 
            <input type="button" value="add" onClick={this.add} />
        </div>
    }
}

export default CrewForm