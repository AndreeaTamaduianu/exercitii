import React, {Component} from 'react'

class Plane extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            maker : this.props.item.maker,
            type : this.props.item.type,
            mass : this.props.item.mass,
            speed : this.props.item.speed
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                maker : this.state.maker,
                type : this.state.type,
                mass : this.state.mass,
                speed : this.state.speed
            })
            this.setState({
                isEditing : false
            })
        }
        this.edit = () => {
            this.setState({isEditing : true})
        }
        this.cancel = () => {
            this.setState({isEditing : false})
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" name="maker" onChange={this.handleChange} value={this.state.maker} />
                    and 
                    <input type="text" name="type" onChange={this.handleChange} value={this.state.type} />
                </h4>
                <h6>with weight 
                    <input type="text" name="mass" onChange={this.handleChange} value={this.state.mass} />
                    and speed 
                    <input type="text" name="speed" onChange={this.handleChange} value={this.state.speed} />
                </h6>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.maker} and {item.type}</h4>
                <h6>with weight {item.mass} and speed {item.speed}</h6>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="select" onClick={this.select} />
                </div>
            </div>           
        }

    }
}

export default Plane