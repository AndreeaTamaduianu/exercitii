import React, {Component} from 'react'

class CrewMember extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            name : this.props.item.name,
            role : this.props.item.role,
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                name : this.state.name,
                role : this.state.role,
            })
            this.setState({
                isEditing : false
            })
        }
        this.edit = () => {
            this.setState({isEditing : true})
        }
        this.cancel = () => {
            this.setState({isEditing : false})
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" name="name" onChange={this.handleChange} value={this.state.name} />
                </h4>
                <h6> 
                    <input type="text" name="role" onChange={this.handleChange} value={this.state.role} />
                </h6>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.name}</h4>
                <h6>{item.role}</h6>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                </div>
            </div>           
        }

    }
}

export default CrewMember