function applyDiscount(vehicles, discount){
    return new Promise(
        function(resolve,reject)
        {
            if(typeof discount=="number")
            {
                var min_price=9999999;
                vehicles.forEach((v)=>{
                    if(typeof v.make=="string" && typeof v.price=="number")
                    {
                        if(v.price<min_price)
                        {
                            min_price=v.price;
                        }
                    }
                    else
                    {
                        reject(new Error("Invalid array format"));
                    }
                });
                if(discount>min_price*0.5)
                {
                    reject("Discount too big");
                }
                resolve(vehicles.map(v=>{
                    return({make:v.make,price:v.price-discount});
                }));
            }
            else
            {
                reject(new Error("Invalid discout"));
            }
        }
    )
}

const app = {
    applyDiscount: applyDiscount
};

module.exports = app;