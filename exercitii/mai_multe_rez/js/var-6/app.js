function applyBonus(employees, bonus) {
    return new Promise(
        function(resolve, reject) {
            if (typeof bonus == "number") {
                var max_salary = 0;
                employees.forEach((e) => {
                    if (typeof e.name == "string" && typeof e.salary == "number") {
                        if (e.salary > max_salary) {
                            max_salary = e.salary;
                        }
                    }
                    else {
                        var reason = "Invalid array format";
                        reject(new Error(reason));
                    }
                });
                if(max_salary*0.1>bonus)
                {
                    reject("Bonus too small");
                }
                resolve(employees.map(e=>{
                    return{name: e.name,salary:e.salary+bonus};
                }));
            }
            else {
                var reason = "Invalid bonus";
                reject(new Error(reason));
            }
        }
    );
}

let app = {
    applyBonus: applyBonus,
};

module.exports = app;
