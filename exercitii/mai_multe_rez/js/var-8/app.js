function addTokens(input, tokens){
    if(typeof input =="string")
    {
        if(input.length>=6)
        {
            tokens.forEach(t=>{
                if(typeof t.tokenName=="string")
                {
                    var str="${"+t.tokenName+"}";
                    if(input.indexOf("...")!=-1)
                    {
                        input=input.replace("...",str);
                    }
                }
                else{
                    throw new Error("Invalid array format");
                }
            });
            return input;
        }
        else
        {
            throw new Error("Input should have at least 6 characters");
        }
    }
    else
    {
        throw new Error("Invalid input");
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;