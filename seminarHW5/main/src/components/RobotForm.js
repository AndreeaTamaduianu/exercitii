import React, { Component } from 'react';
class RobotForm extends Component{ 
    constructor(props) {
        super(props);
        this.state = {
            type: '',
            name: '',
            mass: 0
        };
    this.handleChangePriority = (event) => {
        this.setState({type: event.target.value});
    }
    this.handleChangeTaskName = (event) => {
        this.setState({name: event.target.value});
    }
    this.handleChangeDuration = (event) => {
        this.setState({mass: event.target.value});
    }
    //  this.handleClick = () => {
    //         this.props.onAdd({
    //             name : this.state.name,
    //             type : this.state.type,
    //             mass : this.state.mass
    //         })
    //     }
    // this.handleChange = (evt) => {
    //         this.setState({
    //             [evt.target.name] : evt.target.value
    //         })
    //     }
    }
    render(){
        return(
         <div>
                <h1>Add Robot</h1>
                 <input id="name" type="text" placeholder="name" onChange={this.handleChangeTaskName} name="name" />
                <input id="type" type="text" placeholder="type" onChange={this.handleChangePriority} name="type"/>
        
            <input id="mass" type="number" placeholder="mass" onChange={this.handleChangeDuration} name="mass" />
            <input type="button" value="add" onClick={() => this.props.onAdd({
                type : this.state.type,
                name : this.state.name,
                mass: this.state.mass
            })} />
            
            </div>
)
}
}
// <input type="button" value="add" onClick={this.handleClick} />
 
export default RobotForm;