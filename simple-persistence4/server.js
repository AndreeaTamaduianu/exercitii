const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const sequelize = new Sequelize('student_db', 'app', 'welcome123', {
    dialect : 'mysql'
})

const Student = sequelize.define('student', {
    name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [5, 20]
        }
    },
    age : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min : 18,
            max : 125
        }
    }
})

const Grade = sequelize.define('grade', {
    value : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            min : 1,
            max : 10
        }
    },
    subject : {
        type : Sequelize.ENUM,
        allowNull : false,
        values : ['Programming', 'Operations Research', 'Economics']
    }
})

Student.hasMany(Grade)

const app = express()
app.use(bodyParser.json())

app.post('/sync', async (req, res, next) => {
    try {
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

// get /students?filter=a&page=2&pageSize=10
// get /students?filter=a
// get /students
app.get('/students', async (req, res, next) => {
    try {
        let filter = req.query.filter ? req.query.filter : ''
        let pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10
        let page = req.query.page ? parseInt(req.query.page) : 0
        let students
        if (page || filter){
            students = await Student.findAll({
                where : {
                    name : {
                        [Op.like] : `%${filter}%`
                    }
                },
                limit : pageSize,
                offset : page * pageSize
            })
        }
        else{
            students = await Student.findAll()
        }
        res.status(200).json(students)
    } catch (err) {
        next(err)  
    }
})

app.post('/students', async (req, res, next) => {
    try {
        await Student.create(req.body)
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})

app.get('/students/:id', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.id)
        if (student){
            res.status(200).json(student)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})


app.put('/students/:id', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.id)
        if (student){
            await student.update(req.body)
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.delete('/students/:id', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.id)
        if (student){
            await student.destroy()
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.get('/students/:sid/grades', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.sid, {
            include : [Grade]
        })
        if (student){
            res.status(200).json(student.grades)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }    
})

app.post('/students/:sid/grades', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.sid)
        if (student){
            let grade = req.body
            grade.studentId = student.id
            await Grade.create(grade)
            res.status(201).json({message : 'created'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.get('/students/:sid/grades/:gid', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.sid)
        if(student){
            let grades = await student.getGrades({
                where : {
                    id : req.params.gid
                }
            })
            let grade = grades.shift()
            if (grade){
                res.status(200).json(grade)
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.put('/students/:sid/grades/:gid', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.sid)
        if(student){
            let grades = await student.getGrades({
                where : {
                    id : req.params.gid
                }
            })
            let grade = grades.shift()
            if (grade){
                await grade.update(req.body, {
                    fields : ['value', 'subject']
                })
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.delete('/students/:sid/grades/:gid', async (req, res, next) => {
    try {
        let student = await Student.findByPk(req.params.sid)
        if(student){
            let grades = await student.getGrades({
                where : {
                    id : req.params.gid
                }
            })
            let grade = grades.shift()
            if (grade){
                await grade.destroy()
                res.status(202).json({message : 'accepted'})
            }
            else{
                res.status(404).json({message : 'not found'})
            }
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({message : ':('})
})

app.listen(8080)