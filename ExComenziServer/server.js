const express = require("express")

const bodyParser = require("body-parser")
const Sequelize = require("sequelize")

const cors = require("cors")

const sequelize = new Sequelize('bus_db', 'supermama', 'pdw', {
    dialect: 'mysql'
})


let Bus = sequelize.define('bus',{
    registration : {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            is : /^[A-Z]{1,2}[0-9]{2}[A-Z]{3}$/
        }
    },
    capacity : {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate:{
            isInt : true,
            min : 10
        }
    }
})

let Driver = sequelize.define('driver',{
    name : {
        type: Sequelize.STRING,
        allowNull : false,
        validate:{
            len:[5,20]
        }
    },
    shift:{
        type:Sequelize.ENUM,
        allowNull: false,
        values: ['MORNING', "AFTERNOON","EVENING"]
    }
})

Bus.hasMany(Driver)


const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(express.static('../simple-ui1/build'))
app.post('/sync', async(req,res, next)=>{
    try{
        await sequelize.sync({force:true})
        res.status(201).json({message:'created'})
        
        
    }
    catch(err){
        next(err)
        
        
    }
    
    
})



app.get('/buses', async(req,res,next)=>{
     try{
        let buses = await Bus.findAll()
        res.status(200).json(buses)
        
        
    }
    catch(err){
        next(err)
        
        
    }
    
})
app.post('/buses', async(req,res,next)=>{
     try{
        await Bus.create(req.body)
        res.status(201).json({message:'created'})
        
        
    }
    catch(err){
      
        next(err)
        
    }
})
app.get('/buses/:id', async(req,res, next)=>{
    try {
        let bus = await Bus.findByPk(req.params.id)
        if(bus){
            res.status(200).json(bus)
        }
        else{
            res.status(404).json({message:'not found'})
        }
        
    } catch (err) {
        next(err)
    }
})
app.put('/buses/:id', async(req,res,next)=>{
    try {
        let bus = await Bus.findByPk(req.params.id)
        if(bus){
           await bus.update(req.body)
            res.status(200).json({message:'accepted'})
        }
        else{
            res.status(404).json({message:'not found'})
        }
        
    } catch (err) {
        next(err)
    }
    
})

app.delete('/buses/:id', async(req,res,next)=>{
     try {
        let bus = await Bus.findByPk(req.params.id)
        if(bus){
           await bus.destroy()
            res.status(200).json({message:'accepted'})
        }
        else{
            res.status(404).json({message:'not found'})
        }
        
    } catch (err) {
        next(err)
    }
})

app.get('/buses/:bid/drivers',async(req,res,next)=>{
    
    
    try {
        let bus = await Bus.findByPk(req.params.bid,{include:
            [Driver]})
            if(bus){
                res.status(200).json(bus.drivers)
                
            }
            else{
                res.status(404).json({message:'not found'})
            }
        
        
    } catch (err) {
        next(err)
    }
    
})


app.post('/buses/:bid/drivers',async(req,res,next)=>{
    
     try {
         let bus = await Bus.findByPk(req.params.bid)
         if(bus){
             let driver = req.body
             driver.busId = bus.id
             await Driver.create(driver)
             res.status(201).json({message: 'created'})
         }
         else{
             res.status(404).json({message: 'not found'})
         }
     } catch (err) {
         next(err)
     }
})

app.get('/buses/:bid/drivers/:did',async(req,res,next)=>{
    
    try {
        let bus = await Bus.findByPk(req.params.bid)
        if(bus){
            let drivers = await bus.getDrivers({
                where: {
                id: req.params.did
                    }
                        
            })
            let driver = drivers.shift()
            if(driver){
                res.status(200).json(driver)
            }
            else{
                res.status(404).json({message: 'not found'})
            }
        }
        else{
            res.status(404).json({message: 'not found'})

        }
        
    } catch (err) {
        next(err)
    }
})
app.put('/buses/:bid/drivers/:did',async(req,res,next)=>{
    try {
        let bus = await Bus.findByPk(req.params.bid)
        if(bus){
            let drivers = await bus.getDrivers({
                where: {
                id: req.params.did
                    }
                        
            })
            let driver = drivers.shift()
            if(driver){
                await driver.update(req.body,{
                    fields : ['name', 'shift']
                })
                res.status(202).json({message:'accepted'})
            }
            else{
                res.status(404).json({message: 'not found'})
            }
        }
        else{
            res.status(404).json({message: 'not found'})

        }
        
    } catch (err) {
        next(err)
    }
    
})
app.delete('/buses/:bid/drivers/:did',async(req,res,next)=>{
    
    try {
        let bus = await Bus.findByPk(req.params.bid)
        if(bus){
            let drivers =await bus.getDrivers({
                where: {
                id: req.params.did
                    }
                        
            })
            let driver = drivers.shift()
            if(driver){
                await driver.destroy()
                res.status(202).json({message:'accepted'})
            }
            else{
                res.status(404).json({message: 'not found'})
            }
        }
        else{
            res.status(404).json({message: 'not found'})

        }
        
    } catch (err) {
        next(err)
    }
})



app.use((err,req,res,next)=>{
      console.warn(err)
        res.status(500).json({message:"the hamsters are in trouble"})
    
})
app.listen(8080)


