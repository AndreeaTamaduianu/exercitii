import React, {Component} from 'react'

class Grade extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            value : this.props.item.value,
            subject : this.props.item.subject
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                value : this.state.value,
                subject : this.state.subject
            })
            this.setState({
                isEditing : false
            })
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" value={this.state.value} name="value" onChange={this.handleChange} />
                </h4>
                <h5>age : 
                    <input type="text" value={this.state.subject} name="subject"  onChange={this.handleChange} />
                </h5>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.value}</h4>
                <h5>age : {item.subject}</h5>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                </div>
            </div>
        }
    }
}

export default Grade