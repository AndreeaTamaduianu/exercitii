import React, {Component} from 'react'

class Student extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            name : this.props.item.name,
            age : this.props.item.age
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                name : this.state.name,
                age : this.state.age
            })
            this.setState({
                isEditing : false
            })
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" value={this.state.name} name="name" onChange={this.handleChange} />
                </h4>
                <h5>age : 
                    <input type="text" value={this.state.age} name="age"  onChange={this.handleChange} />
                </h5>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.name}</h4>
                <h5>age : {item.age}</h5>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="select self" onClick={this.select} />
                </div>
            </div>
        }
    }
}

export default Student