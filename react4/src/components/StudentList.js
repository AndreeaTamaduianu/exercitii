import React,{Component} from 'react';
import StudentForm from './StudentForm'
import StudentStore from '../stores/StudentStore'
import Student from './Student'
import StudentDetails from './StudentDetails'

class StudentList extends Component{
    constructor(){
        super()
        this.state = {
            students : [],
            selectedStudent : null
        }
        this.store = new StudentStore()
        this.addStudent = (student) => {
            this.store.addStudent(student)
        }
        this.deleteStudent = (id) => {
            this.store.deleteStudent(id)
        }
        this.saveStudent = (id, student) => {
            this.store.saveStudent(id, student)
        }
        this.selectStudent = (student) =>{
            this.setState({
                selectedStudent : student
            })
        }
        this.cancelSelection = () => {
            this.setState({
                selectedStudent : null
            })
        }
    }
    componentDidMount(){
        this.store.getStudents()
        this.store.emitter.addListener('GET_STUDENTS_SUCCESS', () => {
            this.setState({
                students : this.store.students
            })
        })
    }
    render(){
        if (this.state.selectedStudent){
            return <StudentDetails item={this.state.selectedStudent} onCancel={this.cancelSelection} />
        }
        else{
            return <div>
                <div>
                    {
                        this.state.students.map((e, i) => <Student key={i} item={e} onDelete={this.deleteStudent} onSave={this.saveStudent} onSelect={this.selectStudent} />)
                    }
                </div>
                <StudentForm onAdd={this.addStudent} />
            </div>            
        }
    }
}

export default StudentList
