import React,{Component} from 'react';

class GradeForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            value : '',
            subject : ''
        }
        this.add = (evt) => {
            this.props.onAdd({
                value : this.state.value,
                subject : this.state.subject
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="value" name="value" onChange={this.handleChange} />
            <input type="text" placeholder="subject" name="subject" onChange={this.handleChange} />
            <input type="button" value="+" onClick={this.add} />
        </div>
    }
}

export default GradeForm
