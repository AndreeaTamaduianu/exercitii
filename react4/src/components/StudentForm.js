import React,{Component} from 'react';

class StudentForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            age : ''
        }
        this.add = (evt) => {
            this.props.onAdd({
                name : this.state.name,
                age : this.state.age
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange} />
            <input type="text" placeholder="age" name="age" onChange={this.handleChange} />
            <input type="button" value="+" onClick={this.add} />
        </div>
    }
}

export default StudentForm
