import React,{Component} from 'react'
import GradeStore from '../stores/GradeStore'
import Grade from './Grade'
import GradeForm from './GradeForm'

class StudentDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
            grades : []
        }
        
        this.cancel = () => {
            this.props.onCancel()
        }
            
        this.store = new GradeStore()
        this.addGrade = (grade) => {
            this.store.addGrade(this.props.item.id, grade)
        }
        this.deleteGrade = (gradeId) => {
            this.store.deleteGrade(this.props.item.id, gradeId)
        }
        this.saveGrade = (gradeId, grade) => {
            this.store.saveGrade(this.props.item.id, gradeId, grade)
        }
    }
    componentDidMount(){
        this.store.getGrades(this.props.item.id)
        this.store.emitter.addListener('GET_GRADES_SUCCESS', () => {
            this.setState({
                grades : this.store.grades
            })
        })
    }
    render(){
        let {item} = this.props
        return <div>
            i am the details page for : {item.name}
                <div>
                {
                    this.state.grades.map((e, i) => <Grade key={i} item={e} onDelete={this.deleteGrade} onSave={this.saveGrade} />)
                }
                </div>
                <GradeForm onAdd={this.addGrade} />
            <div>
                <input type="button" value="back" onClick={this.cancel} />
            </div>
        </div>
    }
}

export default StudentDetails