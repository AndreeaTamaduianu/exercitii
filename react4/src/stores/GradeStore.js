import {EventEmitter} from 'fbemitter'
import {SERVER} from '../config'

class GradeStore{
    constructor(){
        this.grades = []
        this.emitter = new EventEmitter()
    }
    async getGrades(studentId){
        try {
            let response = await fetch(`${SERVER}/students/${studentId}/grades`)
            let data = await response.json()
            this.grades = data
            this.emitter.emit('GET_GRADES_SUCCESS')
        } catch (err) {
            console.warn(err)
            this.emitter.emit('GET_GRADES_ERROR')
        }
    }
    async addGrade(studentId, grade){
        try {
            await fetch(`${SERVER}/students/${studentId}/grades`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(grade)
            })
            this.getGrades(studentId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_GRADE_ERROR')
        }
    }
    async deleteGrade(studentId, gradeId){
        try {
            await fetch(`${SERVER}/students/${studentId}/grades/${gradeId}`, {
                method : 'delete'
            })
            this.getGrades(studentId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_GRADE_ERROR')
        }
    }
    async saveGrade(studentId, gradeId, grade){
        try {
            await fetch(`${SERVER}/students/${studentId}/grades/${gradeId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(grade)
            })
            this.getGrades(studentId)
        } catch (err) {
            console.warn(err)
            this.emitter.emit('SAVE_GRADE_ERROR') 
        }
    }
}

export default GradeStore