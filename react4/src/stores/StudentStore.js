import {EventEmitter} from 'fbemitter'
import {SERVER} from '../config'

class StudentStore{
    constructor(){
        this.students = []
        this.emitter = new EventEmitter()
    }
    async getStudents(){
        try {
            let response = await fetch(`${SERVER}/students`)
            let data = await response.json()
            this.students = data
            this.emitter.emit('GET_STUDENTS_SUCCESS')
        } catch (err) {
            console.warn(err)
            this.emitter.emit('GET_STUDENTS_ERROR')
        }
    }
    async addStudent(student){
        try {
            await fetch(`${SERVER}/students`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(student)
            })
            this.getStudents()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_STUDENT_ERROR')
        }
    }
    async deleteStudent(id){
        try {
            await fetch(`${SERVER}/students/${id}`, {
                method : 'delete'
            })
            this.getStudents()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_STUDENT_ERROR')
        }
    }
    async saveStudent(id, student){
        try {
            await fetch(`${SERVER}/students/${id}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(student)
            })
            this.getStudents()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('SAVE_STUDENT_ERROR') 
        }
    }
}

export default StudentStore