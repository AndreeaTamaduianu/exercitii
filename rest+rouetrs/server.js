'use strict'
const express = require('express')
const bodyParser = require('body-parser')

const app = express()
app.use((req, res, next) => {
    console.warn(req.url)
    next()
})
app.use(bodyParser.json())

app.locals.kittens = [{name : 'tim', color : 'orange'}]

let statusRouter = require('./routers/status-router')
let kittenRouter = require('./routers/kitten-router')

app.use('/status', statusRouter)
app.use('/kitten-api', kittenRouter)

app.listen(8080)