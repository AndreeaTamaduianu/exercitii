function getAverageGrade(gradeItems){
    var ok=1;
    gradeItems.forEach(g=>{
        if(typeof(g.grade)!=='number'||g.grade<0)
        {
                ok=0;
        }
    })
    if(ok)
    {
         if(gradeItems.length===0)
        {
            return 0;
        }
        else
        {
            var s=0;
            gradeItems.forEach(g=>s+=parseInt(g.grade));
            return parseInt(s/gradeItems.length);
        } 
    }
    else
    {
        throw Error("Invalid grade");
    }
}

const app = {
    getAverageGrade: getAverageGrade
}

module.exports = app