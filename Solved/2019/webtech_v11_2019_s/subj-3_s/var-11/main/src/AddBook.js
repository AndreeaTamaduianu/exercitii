import React from 'react';

class AddBook extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            bookTitle: '',
            bookType: '',
            bookPrice: 0
        };
    }
    handleChange=(evt)=>{
        this.setState({
             [evt.target.name] : evt.target.value
        })
        
    }
    render(){
        return (
        <div>
        <input type='text' id='book-title' name='bookTitle' onChange={this.handleChange}/>
        <input type='text' id='book-type' name='bookType' onChange={this.handleChange}/>
         <input type='text' id='book-price' name='bookPrice' onChange={this.handleChange}/>
         <input type='button'  value='add book' onClick={this.handleAdd}/>
        </div>
        );
    }

    handleAdd = () => {
        let item = {...this.state};
        this.props.itemAdded(item);
    }
}
export default AddBook;