const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.products = [
    {
        name: "Iphone XS",
        category: "Smartphone",
        price: 5000
    },
    {
        name: "Samsung Galaxy S10",
        category: "Smartphone",
        price: 3000
    },
    {
        name: "Huawei Mate 20 Pro",
        category: "Smartphone",
        price: 3500
    }
];

app.get('/products', (req, res) => {
    res.status(200).json(app.locals.products);
});

app.post('/products', (req, res, next) => {
    var product=req.body
    if (Object.keys(product).length != 0)
    {
        if (product.name && product.price && product.category)
        {
            if (product.price < 0)
            {
                  res.status(500).json({message: 'Price should be a positive number'});   
            }
            else 
            {
                let ok = 0;
                app.locals.products.forEach(input => {
                    if (input.name === product.name && input.price ===product.price && input.category === product.category)
                       ok = 1;   
                })
                
                if (ok==1)
                {
                     res.status(500).json({message: 'Product already exists'});  
                }
                else
                {
                    let product = new Object();
                    product.price = req.body.price;
                    product.name = req.body.name;
                    product.category=req.body.category;
            	
            	    app.locals.products.push(product);
            		res.status(201).json({message : 'Created'})
                }
            }
        }
        else
        {
         res.status(500).json({message: 'Invalid body format'});   
        }
        
    }
    else{
        res.status(500).json({message: 'Body is missing'});
    }
})

module.exports = app;