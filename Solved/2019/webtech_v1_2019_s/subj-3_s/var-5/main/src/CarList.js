import React from 'react';
import {AddCar} from './AddCar'
export class CarList extends React.Component {
    constructor(){
        super();
        this.state = {
            cars: []
        };
    }   
    onAddingCar=(car)=>{
      this.setState({
          cars:[...this.state.cars,car]
      })  
    }
    render(){
        return (
            <div>
            <AddCar onAdd={this.onAddingCar}/>
            </div>
        )
    }
}
