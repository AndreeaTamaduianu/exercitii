import React from 'react';
import AddEmployee from './AddEmployee'

export class EmployeeList extends React.Component {
    constructor(){
        super();
        this.state = {
            employees: []
        };
        
        this.addEmployee = (employee) =>{
            this.state.employees.push(employee)
        }
    }

    render(){
        return(
            <div>
                <AddEmployee onAdd={this.addEmployee} />
            </div>
        )
    }
}