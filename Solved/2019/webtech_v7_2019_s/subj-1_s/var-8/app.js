function addTokens(input, tokens){
    if(typeof input != "string"){
        throw new Error("Invalid input");
    }
    if(input.length < 6){
        throw new Error("Input should have at least 6 characters");
    }
    var ok = 1;
    tokens.forEach(tok => {
        if(typeof tok.tokenName !== "string"){
            ok=0;
        }
    })
    if(ok==0){
        throw new Error("Invalid array format");
    }
    
    var returned = input;
    var splitted = input.split(" ");
    
    if(returned.includes("...")){
       splitted.forEach(split =>{
           tokens.forEach(tok =>{
               if(split==="..."){
                   returned = returned.replace(split, "${" + tok.tokenName + "}");
               }
           })
       })
    }
    
     return returned;
}

const app = {
    addTokens: addTokens
}

module.exports = app;