import React from 'react';

export default class AddTask extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            taskName: '',
            taskPriority: 'low',
            taskDuration: 0
        };
    }

    render(){
        return (
        <div>
             <input type="text" name="task-name" id="task-name" onChange={this.handleChange} />
            	<input type="text" name="task-priority" id="task-priority" onChange={this.handleChange} />
            	<input type="text"  name="task-duration" id="task-duration" onChange={this.handleChange} />
            	<input type="button" value="add task" onClick={this.addTask} />
        </div>
        );
    }

    addTask = () => {
        let task = {...this.state};
        this.props.taskAdded(task);
    }
}

