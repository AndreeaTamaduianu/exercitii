import React from 'react';

 class AddTransaction extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            transactionNumber: '',
            transactionType: '',
            amount: 0
        };
          this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }

    render(){
        return (
        <div>
        <input type='text' id='transaction-number' name='transactionNumber' onChange={this.handleChange}/>
        <input type='text' id='transaction-type' name='transactionType' onChange={this.handleChange}/>
         <input type='text' id='transaction-amount' name='amount' onChange={this.handleChange}/>
         <input type='button'  value='add transaction' onClick={this.handleAdd}/>
        
        </div>
        );
    }

    
    handleAdd = () => {
        let item = {...this.state};
        this.props.itemAdded(item);
    }
}
export default AddTransaction;