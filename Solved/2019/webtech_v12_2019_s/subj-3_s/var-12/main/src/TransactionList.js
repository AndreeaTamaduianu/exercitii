import React from 'react';
import AddTransaction from './AddTransaction';
class TransactionList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: []
          };
    }
    onAdd=(transaction)=>{
        this.setState({
            data:[...this.state.data,transaction]
        })
    }
    render() {
        return (
            <div>
            <AddTransaction itemAdded={this.onAdd}/>
            </div>
        );
    }
}
export default TransactionList;