import React from 'react';
import TransactionList from './TransactionList';

class App extends React.Component{

  render() {
    return (
      <div>
        <TransactionList />
      </div>
    );
  }
}
export default App;