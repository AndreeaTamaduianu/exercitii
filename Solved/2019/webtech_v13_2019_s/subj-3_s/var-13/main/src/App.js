import React from 'react';
import VacationList from './VacationList';

class App extends React.Component{

  render() {
    return (
      <div>
        <VacationList />
      </div>
    );
  }
}
export default  App;