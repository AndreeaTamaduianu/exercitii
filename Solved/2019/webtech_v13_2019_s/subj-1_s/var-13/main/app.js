function toCamelCase(input){
    
    var words;
    if(input)
    {
        if(typeof(input)==='number')
        {
            throw Error("Input must be a string primitive or a string object")
        }
        else
        {
        if(input.includes(' '))
        {
         var res='';
         words=input.split(' ');
         
         words.forEach(word=>{
            var converted= word.slice(0,1).toUpperCase()+word.slice(1,word.length);
            res+=converted;
         })
         res=res.slice(0,1).toLowerCase()+res.slice(1,res.length);
         return res;
        }
        else 
            if(input.includes('-'))
            {
                 res='';
                 words=input.split('-');
                 
                 words.forEach(word=>{
                    var converted= word.slice(0,1).toUpperCase()+word.slice(1,word.length);
                    res+=converted;
                 })
                 res=res.slice(0,1).toLowerCase()+res.slice(1,res.length);
                 return res;
            }
                else if(input.includes('_'))
                {
                     res='';
                     words=input.split('_');
                     
                     words.forEach(word=>{
                        var converted= word.slice(0,1).toUpperCase()+word.slice(1,word.length);
                        res+=converted;
                     })
                     res=res.slice(0,1).toLowerCase()+res.slice(1,res.length);
                     return res;
                }
        }
    }
    else
    {
        throw Error("Input must be a string primitive or a string object")
    }
    
    
    

    
}

const app = {
  toCamelCase: toCamelCase
}

module.exports = app