import React from 'react';
import AddCoupon from './AddCoupon'

export class CouponList extends React.Component {
    constructor(){
        super();
        this.state = {
            coupons: []
        };
        
       
    }
     addCoupon = (coupon) =>{
         this.setState({
             coupons:[...this.state.coupons,coupon]
         })
            
        }

    render(){
        return(
            <div>
                <AddCoupon onAdd={this.addCoupon}/>
            </div>
        )
    }
}