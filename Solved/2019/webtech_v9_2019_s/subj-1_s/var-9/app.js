function applyBlackFriday(products, discount){
    return new Promise((resolve, reject) =>{
        if(typeof discount !== "number"){
            reject(new Error("Invalid discount"));
        }
        if(discount<=0 || discount > 10){
            reject(new Error("Discount not applicable"));
        }
        var ok = 1;
        products.forEach(prod =>{
            if(typeof prod.name !== "string" || typeof prod.price !=="number"){
                ok=0;
            }
        })
        if(ok==0){
            reject(new Error("Invalid array format"))
        }
        else{
        var newArray = [];
            products.forEach(prod =>{
                var obj = new Object();
                obj.price = prod.price - discount*prod.price/100;
                obj.name = prod.name;
                newArray.push(obj);
            })
            resolve(newArray);
        }
    })
}

const app = {
    applyBlackFriday: applyBlackFriday
};
module.exports = app;