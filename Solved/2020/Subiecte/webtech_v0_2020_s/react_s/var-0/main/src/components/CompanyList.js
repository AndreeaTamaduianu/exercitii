import React, { Component } from 'react'
import CompanyStore from '../stores/CompanyStore'
import Company from './Company'
import CompanyForm from './CompanyForm'
class CompanyList extends Component {
    constructor(){
        super()
        this.state = {
            companies : [],
            selectedCompany : null
        }
        	this.store = new CompanyStore()
        this.addCompany = (company) => {
						this.store.addOne(company)
						}
        this.deleteCompany = (id) => {
            this.store.deleteOne(id)
        }
        this.saveCompany = (id, Company) => {
            this.store.saveCompany(id, Company)
        }
        this.selectCompany = (Company) =>{
            this.setState({
                selectedCompany : Company
            })
        }
        this.cancelSelection = () => {
            this.setState({
                selectedCompany : null
            })
        }
    }
	componentDidMount(){
		this.setState({
			companies : this.store.getAll()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				companies : this.store.getAll()
			})			
		})
	}
    render(){
        return <div>
                <div>
                    {
                        this.state.companies.map((e, i) => <Company key={i} item={e} onDelete={this.deleteCompany} onSave={this.saveCompany} onSelect={this.selectCompany} />)
                    }
                </div>
                <CompanyForm onAdd={this.addCompany} />
            </div>             
        
    }
}

export default CompanyList

