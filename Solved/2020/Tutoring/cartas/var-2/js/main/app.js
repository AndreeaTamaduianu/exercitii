function applyDiscount(vehicles, discount){
    return new Promise((resolve, reject) => {
       if(typeof discount !== 'number') {
           throw new Error('Invalid discount');
       }
       if(vehicles.find(vehicle => typeof vehicle.make !== 'string' || typeof vehicle.price !== 'number')) {
           throw new Error('Invalid array format');
       }
       const min = Math.min(...vehicles.map(vehicle => vehicle.price));
       if(discount > min / 2) {
           throw new Error('Discount too big');
       }
       const result = vehicles.map(vehicle => {
           vehicle.price -= discount;
           return vehicle;
       })
       resolve(result);
    })   
}

const app = {
    applyDiscount: applyDiscount
};

module.exports = app;