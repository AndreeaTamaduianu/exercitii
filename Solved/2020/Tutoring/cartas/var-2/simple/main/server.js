const express = require("express")
const data = require('./account.json');
const path = require('path');
const app = express()
app.use(express.static(path.join(__dirname, 'public')));
app.listen(8080);

app.get('/data', (req, res) => {
    res.status(200).send(data);
})

module.exports = app;