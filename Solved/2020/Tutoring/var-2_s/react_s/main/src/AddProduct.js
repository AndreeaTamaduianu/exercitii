import React from 'react';

export class AddProduct extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            category: '',
            price: ''
        };
    }

    addProduct = () => {
        let product = {
            name: this.state.name,
            category: this.state.category,
            price: this.state.price
        };
        this.props.onAdd(product);
    }

    render(){
        return (
            <div>
            <input type='text' name='name' id='name'/>
            <input type='text' name='category' id='category'/>
            <input type='text' name='price' id='price'/>
            <input type='button' value='add product' onClick={this.addProduct}/>
            </div>
        )
    }
}